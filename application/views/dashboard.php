<div class="container-fluid">

    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="<?php echo base_url('assets/img/slidet1.jpg') ?>" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="<?php echo base_url('assets/img/slide2.jpg') ?>" class="d-block w-100" alt="...">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
        </a>
    </div>

    <div class="row text-center mt-5">

        <?php foreach ($barang as $brg) : ?>

            <div class="card ml-3" style="width: 16rem;">
                <img src="<?php echo base_url() . '/upload/' . $brg->gambar ?>" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title mb-1"><?php echo $brg->nama_barang ?></h5>
                    <small><?php echo $brg->deskripsi ?></small><br>
                    <a href="#" class="btn btn-sm btn-primary"><i class="fas fa-shopping-basket "></i></a>
                    <a href="#" class="btn btn-sm btn-success">Detail</a>
                    <span class="badge rounded-pill bg-light text-dark">Rp. <?php echo $brg->harga ?></span>
                </div>
            </div>


        <?php endforeach; ?>
    </div>
</div>